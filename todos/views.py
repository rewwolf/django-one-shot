from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList
from todos.forms import TodoListForm


def todo_list_list(request):
    todo_lists = TodoList.objects.all()
    context = {
        "todo_list_list": todo_lists,
    }
    return render(request, "todos/list.html", context)


# Create a view that shows the details of a particular to-do list,
# including its tasks (below)


def todo_list_detail(request, id):
    todo_item = get_object_or_404(TodoList, id=id)
    context = {
        "list_details": todo_item,
    }
    return render(request, "todos/details.html", context)

# Create a create view for the TodoList model that will show the name
# field in the form and handle the form submission to create a new
# TodoList (below)


def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.id)
    else:
        form = TodoListForm()
    context = {
        "form": form
    }
    return render(request, "todos/create.html", context)


def todo_list_update(request, id):
    list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=list)
        if form.is_valid():
            edit = form.save()
            # redirect back to the page that shows the post
            return redirect("todo_list_detail", id=edit.id)
    else:
        form = TodoListForm(instance=list)

    context = {
        "form": form,
    }
    return render(request, "todos/edit.html", context)
